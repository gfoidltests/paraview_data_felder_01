# state file generated using paraview version 5.0.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1563, 770]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [-0.5, -0.5, -0.5]
renderView1.StereoType = 0
renderView1.CameraPosition = [13.101095128219209, 8.176475414471756, -13.361200576499016]
renderView1.CameraFocalPoint = [-1.3715420236097162, -0.8002407606649907, -1.827274656441986]
renderView1.CameraViewUp = [-0.2352866293455489, 0.887737197218819, 0.39568026324961647]
renderView1.CameraParallelScale = 7.794228634059948
renderView1.Background = [0.32, 0.34, 0.43]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'CSV Reader'
felder_01csv = CSVReader(FileName=['D:\\Work-Git\\gfoidl\\ParaView\\ParaView_Data_Felder_01\\bin\\Debug\\felder_01.csv'])
felder_01csv.FieldDelimiterCharacters = ';'

# create a new 'Table To Structured Grid'
tableToStructuredGrid1 = TableToStructuredGrid(Input=felder_01csv)
tableToStructuredGrid1.WholeExtent = [0, 9, 0, 9, 0, 9]
tableToStructuredGrid1.XColumn = 'x'
tableToStructuredGrid1.YColumn = 'y'
tableToStructuredGrid1.ZColumn = 'z'

# create a new 'Calculator'
vektor = Calculator(Input=tableToStructuredGrid1)
vektor.ResultArrayName = 'u_'
vektor.Function = 'u_x*iHat+u_y*jHat+u_z*kHat'

# create a new 'Stream Tracer'
streamTracer1 = StreamTracer(Input=vektor,
    SeedType='Point Source')
streamTracer1.Vectors = ['POINTS', 'u_']
streamTracer1.MaximumStreamlineLength = 9.0

# init the 'Point Source' selected for 'SeedType'
streamTracer1.SeedType.Center = [-0.5, -0.5, -0.5]
streamTracer1.SeedType.Radius = 0.9

# create a new 'Glyph'
glyph1 = Glyph(Input=streamTracer1,
    GlyphType='Arrow')
glyph1.Scalars = ['POINTS', 'AngularVelocity']
glyph1.Vectors = ['POINTS', 'u_']
glyph1.ScaleFactor = 0.8786443948745728
glyph1.GlyphTransform = 'Transform2'

# create a new 'Ribbon'
ribbon1 = Ribbon(Input=streamTracer1)
ribbon1.Scalars = ['POINTS', 'AngularVelocity']
ribbon1.Vectors = ['POINTS', 'Normals']
ribbon1.Width = 0.08786443948745727

# create a new 'Axes'
axes1 = Axes()

# create a new 'Tube'
tube1 = Tube(Input=streamTracer1)
tube1.Scalars = ['POINTS', 'AngularVelocity']
tube1.Vectors = ['POINTS', 'u_']
tube1.Radius = 0.08786443948745727

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'AngularVelocity'
angularVelocityLUT = GetColorTransferFunction('AngularVelocity')
angularVelocityLUT.RGBPoints = [-30.957464226041854, 0.0, 0.0, 1.0, 2.9130020201115805, 1.0, 0.0, 0.0]
angularVelocityLUT.ColorSpace = 'HSV'
angularVelocityLUT.NanColor = [0.498039215686, 0.498039215686, 0.498039215686]
angularVelocityLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'AngularVelocity'
angularVelocityPWF = GetOpacityTransferFunction('AngularVelocity')
angularVelocityPWF.Points = [-30.957464226041854, 0.0, 0.5, 0.0, 2.9130020201115805, 1.0, 0.5, 0.0]
angularVelocityPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from glyph1
glyph1Display = Show(glyph1, renderView1)
# trace defaults for the display properties.
glyph1Display.ColorArrayName = ['POINTS', 'AngularVelocity']
glyph1Display.LookupTable = angularVelocityLUT
glyph1Display.GlyphType = 'Arrow'
glyph1Display.SetScaleArray = ['POINTS', 'AngularVelocity']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'AngularVelocity']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [0.006785521749407053, 0.0, 0.5, 0.0, 0.028818512335419655, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [0.006785521749407053, 0.0, 0.5, 0.0, 0.028818512335419655, 1.0, 0.5, 0.0]

# show color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for angularVelocityLUT in view renderView1
angularVelocityLUTColorBar = GetScalarBar(angularVelocityLUT, renderView1)
angularVelocityLUTColorBar.Position = [0.85, 0.52]
angularVelocityLUTColorBar.Position2 = [0.12, 0.42999999999999994]
angularVelocityLUTColorBar.Title = 'AngularVelocity'
angularVelocityLUTColorBar.ComponentTitle = ''