﻿using System;

namespace ConsoleApplication1
{
	public abstract class ResultWriter : IResultWriter
	{
		public abstract void WriteResult(Data[][][] result, string fileNameWithoutExtension);
		//---------------------------------------------------------------------
		protected void WriteDataItems(Data[][][] result, Action<Data> lineAction)
		{
			for (int i = 0; i < result.Length; ++i)
				for (int j = 0; j < result[i].Length; ++j)
					for (int k = 0; k < result[i][j].Length; ++k)
						lineAction(result[i][j][k]);
		}
	}
}