﻿using System;
using System.Globalization;
using System.IO;

namespace ConsoleApplication1
{
	public class InvariantStreamWriter : StreamWriter
	{
		public InvariantStreamWriter(string path, bool newLineAsUnix = true)
			: base(path)
		{
			if (newLineAsUnix)
				this.NewLine = "\n";
		}
		//---------------------------------------------------------------------
		public override IFormatProvider FormatProvider
		{
			get
			{
				return CultureInfo.InvariantCulture;
			}
		}
	}
}