﻿using System.IO;

namespace ConsoleApplication1
{
	public class CsvResultWriter : ResultWriter, IResultWriter
	{
		public override void WriteResult(Data[][][] result, string fileNameWithoutExtension)
		{
			using (StreamWriter sw = new InvariantStreamWriter(Path.ChangeExtension(fileNameWithoutExtension, "csv")))
			{
				sw.WriteLine("x;y;z;u_x;u_y;u_z;divU");

				this.WriteDataItems(result, r =>
					sw.WriteLine(
						"{0};{1};{2};{3};{4};{5};{6}",
						r.x, r.y, r.z,
						r.u.x, r.u.y, r.u.z,
						r.DivU));
			}
		}
	}
}