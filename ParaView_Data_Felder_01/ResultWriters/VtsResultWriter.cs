﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace ConsoleApplication1
{
	public class VtsResultWriter : ResultWriter, IResultWriter
	{
		public override void WriteResult(Data[][][] result, string fileNameWithoutExtension)
		{
			XElement xml = this.CreateXml(result);

			xml.Save(Path.ChangeExtension(fileNameWithoutExtension, "vts"));
		}
		//---------------------------------------------------------------------
		private XElement CreateXml(Data[][][] result)
		{
			int nx = result.Length;
			int ny = result[0].Length;
			int nz = result[1].Length;
			int n = nx * ny * nz;

			XElement xml = new XElement("VTKFile",
				new XAttribute("type", "StructuredGrid"),
				new XAttribute("version", "1.0"),
				new XAttribute("byte_order", "LittleEndian"),
				new XAttribute("header_type", "UInt64"),
				new XElement("StructuredGrid",
					new XAttribute("WholeExtent", string.Format("0 {0} 0 {1} 0 {2}", nx - 1, ny - 1, nz - 1)),
					new XElement("Piece",
						new XAttribute("Extent", string.Format("0 {0} 0 {1} 0 {2}", nx - 1, ny - 1, nz - 1))
					)
				)
			);

			XElement piece = xml.Descendants("Piece").First();
			piece.Add(this.GetPoints(result));
			piece.Add(this.GetPointData(result));
			piece.Add(new XElement("CellData"));

			return xml;
		}
		//---------------------------------------------------------------------
		private XElement GetPoints(Data[][][] result)
		{
			XElement dataArray = new XElement("DataArray",
				new XAttribute("type", "Float64"),
				new XAttribute("Name", "Points"),
				new XAttribute("NumberOfComponents", 3),
				new XAttribute("format", "ascii")
			);

			double min = double.MaxValue;
			double max = double.MinValue;
			StringBuilder sb = new StringBuilder();

			this.WriteDataItems(result, r =>
			{
				min = Math.Min(min, Math.Min(Math.Min(r.x, r.y), r.z));
				max = Math.Max(max, Math.Max(Math.Max(r.x, r.y), r.z));

				sb.AppendFormat(CultureInfo.InvariantCulture, "{0} {1} {2}\n", r.x, r.y, r.z);
			});

			dataArray.Value = sb.ToString();
			dataArray.Add(
				new XAttribute("RangeMin", min),
				new XAttribute("RangeMax", max));

			XElement points = new XElement("Points", dataArray);

			return points;
		}
		//---------------------------------------------------------------------
		private XElement GetPointData(Data[][][] result)
		{
			XElement dataArrayU = this.GetDataArrayU(result);
			XElement dataArrayDivU = this.GetDataArrayDivU(result);

			XElement pointData = new XElement("PointData",
				new XAttribute("Vectors", "u_"),
				dataArrayU,
				dataArrayDivU
			);

			return pointData;
		}
		//---------------------------------------------------------------------
		private XElement GetDataArrayU(Data[][][] result)
		{
			XElement dataArrayU = new XElement("DataArray",
				new XAttribute("type", "Float64"),
				new XAttribute("Name", "u_"),
				new XAttribute("NumberOfComponents", 3),
				new XAttribute("format", "ascii")
			);

			double min = double.MaxValue;
			double max = double.MinValue;
			StringBuilder sb = new StringBuilder();

			this.WriteDataItems(result, r =>
			{
				min = Math.Min(min, Math.Min(Math.Min(r.u.x, r.u.y), r.u.z));
				max = Math.Max(max, Math.Max(Math.Max(r.u.x, r.u.y), r.u.z));

				sb.AppendFormat(CultureInfo.InvariantCulture, "{0} {1} {2}\n", r.u.x, r.u.y, r.u.z);
			});

			dataArrayU.Value = sb.ToString();
			dataArrayU.Add(
				new XAttribute("RangeMin", min),
				new XAttribute("RangeMax", max));

			return dataArrayU;
		}
		//---------------------------------------------------------------------
		private XElement GetDataArrayDivU(Data[][][] result)
		{
			XElement dataArrayDivU = new XElement("DataArray",
				new XAttribute("type", "Float64"),
				new XAttribute("Name", "divU"),
				new XAttribute("format", "ascii")
			);

			double min = double.MaxValue;
			double max = double.MinValue;
			StringBuilder sb = new StringBuilder();

			this.WriteDataItems(result, r =>
			{
				min = Math.Min(min, r.DivU);
				max = Math.Max(max, r.DivU);

				sb.AppendFormat(CultureInfo.InvariantCulture, "{0}\n", r.DivU);
			});

			dataArrayDivU.Value = sb.ToString();
			dataArrayDivU.Add(
				new XAttribute("RangeMin", min),
				new XAttribute("RangeMax", max));

			return dataArrayDivU;
		}
	}
}