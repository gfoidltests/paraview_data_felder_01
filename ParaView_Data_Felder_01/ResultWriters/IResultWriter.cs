﻿namespace ConsoleApplication1
{
	public interface IResultWriter
	{
		void WriteResult(Data[][][] result, string fileNameWithoutExtension);
	}
}