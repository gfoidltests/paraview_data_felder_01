﻿using System;
using System.IO;

namespace ConsoleApplication1
{
	public class VtkResultWriter : ResultWriter, IResultWriter
	{
		public override void WriteResult(Data[][][] result, string fileNameWithoutExtension)
		{
			int nx = result.Length;
			int ny = result[0].Length;
			int nz = result[1].Length;

			using (StreamWriter sw = new InvariantStreamWriter(Path.ChangeExtension(fileNameWithoutExtension, "vtk")))
			{
				sw.WriteLine("# vtk DataFile Version 4.0");
				sw.WriteLine("Felder 01");
				sw.WriteLine("ASCII");

				sw.WriteLine("DATASET STRUCTURED_GRID");
				sw.WriteLine("DIMENSIONS {0} {1} {2}", nx, ny, nz);

				sw.WriteLine();
				sw.WriteLine("POINTS {0} double", nx * ny * nz);
				this.WriteDataItems(result, r => sw.WriteLine("  {0:N} {1:N} {2:N}", r.x, r.y, r.z));

				sw.WriteLine();
				sw.WriteLine("POINT_DATA {0}", nx * ny * nz);
				sw.WriteLine("  VECTORS u_ double");
				this.WriteDataItems(result, r => sw.WriteLine("    {0:N} {1:N} {2:N}", r.u.x, r.u.y, r.u.z));

				sw.WriteLine();
				sw.WriteLine("FIELD FieldData 1");
				sw.WriteLine("  divU 1 {0} double", nx * ny * nz);
				this.WriteDataItems(result, r => sw.WriteLine("    {0:N}", r.DivU));
			}
		}
	}
}