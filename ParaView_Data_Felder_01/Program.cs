﻿using System;
namespace ConsoleApplication1
{
	public struct Vector
	{
		public double x { get; set; }
		public double y { get; set; }
		public double z { get; set; }
	}
	//---------------------------------------------------------------------
	public struct Data
	{
		public double x { get; set; }
		public double y { get; set; }
		public double z { get; set; }
		public Vector u { get; set; }
		public double DivU { get; set; }
	}
	//-------------------------------------------------------------------------
	class Program
	{
		static void Main(string[] args)
		{
			Data[][][] result = Compute();

			Action<IResultWriter> writeResult = rw => rw.WriteResult(result, "felder_01");

			IResultWriter resultWriter = new CsvResultWriter();
			writeResult(resultWriter);

			resultWriter = new VtkResultWriter();
			writeResult(resultWriter);

			resultWriter = new VtsResultWriter();
			writeResult(resultWriter);
		}
		//---------------------------------------------------------------------
		private static Data[][][] Compute()
		{
			const int n = 10;
			const double xMin = -5;
			const double xMax = +5;

			Data[][][] result = new Data[n][][];

			for (int i = 0; i < n; ++i)
			{
				result[i] = new Data[n][];

				double x = xMin + (xMax - xMin) / n * i;

				for (int j = 0; j < n; ++j)
				{
					result[i][j] = new Data[n];

					double y = xMin + (xMax - xMin) / n * j;

					for (int k = 0; k < n; ++k)
					{
						double z = xMin + (xMax - xMin) / n * k;

						Vector u = new Vector();
						u.x = Math.Sin(y * y + z * z);
						u.y = Math.Sin(x * x + y * y);
						u.z = Math.Sin(y * y + z * z);

						result[i][j][k].x = x;
						result[i][j][k].y = y;
						result[i][j][k].z = z;

						result[i][j][k].u = u;
						result[i][j][k].DivU = 0 + 0 + 0;
					}
				}
			}

			return result;
		}
	}
}